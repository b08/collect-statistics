# @b08/collect-statistics, seeded from @b08/library-seed, library type: infrastructure
method to collect package statistics from gitlab, loc, tests and coverage
Finds "release" job output in json format for each package under the account