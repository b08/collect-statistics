import { LibraryStatistic } from "./type";

const regex = /Library statistics:\s+({.*})\s*[\r\n]+/;

export function parsePipelineJobOutput(file: string): LibraryStatistic {
  if (file == null) { return null; }
  const match = file.match(regex);
  if (match == null) { return null; }
  try {
    const model = JSON.parse(match[1]);
    return <LibraryStatistic>model;
  } catch (err) {
    return null;
  }
}
