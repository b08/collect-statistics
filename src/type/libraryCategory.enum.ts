export enum Category {
  support = "support",
  dry = "dry",
  feature = "feature",
  generator = "generator",
  infrastructure = "infrastructure"
}
