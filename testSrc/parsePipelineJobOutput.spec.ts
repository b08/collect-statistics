import { test } from "@b08/test-runner";
import { parsePipelineJobOutput } from "../src/parsePipelineJobOutput";

test("parsePipelineJobOutput", expect => {
  // arrange
  const src = `
    tests: [92m5 passed[0m, 5 total
    assertions: [92m5 passed[0m, 5 total

    [16:12:53] Finished 'runTestsLatest' after 9.23 s
    [16:12:53] Starting 'collectCoverage'...
    [16:12:57] Finished 'collectCoverage' after 4.19 s
    [16:12:57] Starting 'displayCoverage'...
    --------------|---------|----------|---------|---------|-------------------
    file          | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
    --------------|---------|----------|---------|---------|-------------------
    all files     |     100 |      100 |     100 |     100 |
     generator.ts |     100 |      100 |     100 |     100 |
    --------------|---------|----------|---------|---------|-------------------

    =============================== Coverage summary ===============================
    statements   : 100% ( 23/23 )
    branches     : 100% ( 9/9 )
    functions    : 100% ( 8/8 )
    lines        : 100% ( 22/22 )
    ================================================================================
    [16:13:00] Finished 'displayCoverage' after 3.47 s
    [16:13:00] Starting 'postMetrics'...
    tag from package.json: 1.0.0
    latest found git tag: 1.0.1

    Library statistics:  {"library":{"name":"@b08/generator","npmUrl":"https://www.npmjs.com/package/@b08/generator","gitRepo":"git@gitlab.com:b08/generator.git","category":"generator"},"version":{"major":1,"minor":0,"patch":2,"publishedAt":{"nDate":20191202}},"metrics":{"loc":58,"tests":5,"coverage":100}}
    [16:13:01] Finished 'postMetrics' after 792 ms
    [16:13:01] Finished 'testLatest' after 26 s`;

  const expected: any = {
    library:
    {
      name: "@b08/generator",
      npmUrl: "https://www.npmjs.com/package/@b08/generator",
      gitRepo: "git@gitlab.com:b08/generator.git",
      category: "generator"
    },
    version: { major: 1, minor: 0, patch: 2, publishedAt: { nDate: 20191202 } },
    metrics: { loc: 58, tests: 5, coverage: 100 }
  };

  // act
  const result = parsePipelineJobOutput(src);

  // assert
  expect.deepEqual(result, expected);
});
